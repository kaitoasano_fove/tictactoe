const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

const GAME_DOESNT_EXIST = -2;
const GAME_NOT_STARTED = -3;
const GAME_ENDED = -4;
const GAME_ONGOING = -5;
const WRONG_TURN = -6;
const INVALID_LOCATION = -7;
const GAME_ENDED_DRAW = 10;

let games = {};
let gameIdCounter = 0;
let playerIdCounter = 0;

function createGame() {
  let gameId = gameIdCounter++;
  games[gameId] = {
    players: [],
    board: [
      [-1, -1, -1],
      [-1, -1, -1],
      [-1, -1, -1],
    ],
    turn: 0,
    ended: false,
  };
  return gameId;
}

function addPlayer(gameId) {
  if (!(gameId in games)) {
    return GAME_DOESNT_EXIST;
  }
  if (games[gameId].players.length >= 2) {
    return games[gameId].ended ? GAME_ENDED : GAME_ONGOING;
  }
  let playerId = playerIdCounter++;
  games[gameId].players.push(playerId);
  return playerId;
}

function makeMove(gameId, playerId, boardX, boardY) {
  if (!(gameId in games)) {
    return GAME_DOESNT_EXIST;
  }
  if (games[gameId].players.length < 2) {
    return GAME_NOT_STARTED;
  }
  if (games[gameId].ended) {
    return GAME_ENDED;
  }
  if (games[gameId].players[games[gameId].turn] !== playerId) {
    return WRONG_TURN;
  }
  if (
    boardX < 0 ||
    boardX > 2 ||
    boardY < 0 ||
    boardY > 2 ||
    games[gameId].board[boardX][boardY] !== -1
  ) {
    return INVALID_LOCATION;
  }
  games[gameId].board[boardX][boardY] = playerId;
  if (checkWin(games[gameId].board, playerId)) {
    games[gameId].ended = true;
    return playerId;
  }
  if (games[gameId].board.flat().every((cell) => cell !== -1)) {
    games[gameId].ended = true;
    return GAME_ENDED_DRAW;
  }
  games[gameId].turn = (games[gameId].turn + 1) % 2;
  return GAME_ONGOING;
}

function checkWin(board, playerId) {
  let winLines = [
    [
      [0, 0],
      [0, 1],
      [0, 2],
    ],
    [
      [1, 0],
      [1, 1],
      [1, 2],
    ],
    [
      [2, 0],
      [2, 1],
      [2, 2],
    ],
    [
      [0, 0],
      [1, 0],
      [2, 0],
    ],
    [
      [0, 1],
      [1, 1],
      [2, 1],
    ],
    [
      [0, 2],
      [1, 2],
      [2, 2],
    ],
    [
      [0, 0],
      [1, 1],
      [2, 2],
    ],
    [
      [0, 2],
      [1, 1],
      [2, 0],
    ],
  ];
  return winLines.some((line) =>
    line.every(([x, y]) => board[x][y] === playerId)
  );
}

function printBoard(gameId) {
  const game = games[gameId];
  for (let i = 0; i < 3; i++) {
    let row = "";
    for (let j = 0; j < 3; j++) {
      if (game.board[i][j] === -1) {
        row += "_ ";
      } else if (game.board[i][j] === game.players[0]) {
        row += "X ";
      } else if (game.board[i][j] === game.players[1]) {
        row += "O ";
      }
    }
    console.log(row);
  }
}

function playTurn(gameId, playerId) {
  console.log(`Player ${playerId + 1}, it's your turn.`);
  printBoard(gameId);
  readline.question(
    `Player ${playerId + 1}, enter your move (row column): `,
    (move) => {
      const [row, column] = move.split(" ").map(Number);
      if (
        isNaN(row) ||
        isNaN(column) ||
        row < 1 ||
        row > 3 ||
        column < 1 ||
        column > 3
      ) {
        console.log("Invalid move. Try again.");
        playTurn(gameId, playerId);
        return;
      }
      const result = makeMove(gameId, playerId, row - 1, column - 1);
      if (result === GAME_ONGOING) {
        playTurn(gameId, (playerId + 1) % 2);
      } else if (result < 0) {
        console.log("Invalid move. Try again.");
        playTurn(gameId, playerId);
      } else if (result === GAME_ENDED_DRAW) {
        printBoard(gameId);
        console.log("The game is a draw.");
        readline.close();
      } else {
        printBoard(gameId);
        console.log(`Player ${playerId + 1} wins!`);
        readline.close();
      }
    }
  );
}

function startGame() {
  const gameId = createGame();
  const player1Id = addPlayer(gameId);
  const player2Id = addPlayer(gameId);
  if (player1Id < 0 || player2Id < 0) {
    console.log("Error: Unable to start the game.");
    return;
  }
  playTurn(gameId, player1Id);
}

startGame();
