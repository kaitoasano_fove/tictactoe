# TicTacToe

This is a simple command-line TicTacToe game implemented in JavaScript. It allows two players to take turns and make moves on a 3x3 board. 

## Getting Started

To play the game, follow these steps:

1. Clone the repository or download the source code.

2. Open a terminal or command prompt and navigate to the project directory.

3. Run the following command to install the necessary dependencies:
   ```
   npm install
   ```

4. Start the game by running the following command:
   ```
   node TicTacToeCLI.js
   ```

## How to Play

- The game is played by two players, represented by 'X' and 'O'.

- The board is a 3x3 grid. Each cell on the board is initially empty.

- Players take turns entering their moves on the board.

- To make a move, enter the row and column numbers when prompted. The input should be in the format: `row column`.

- The row and column numbers should be integers between 1 and 3, representing the position on the board.

- The game will continue until one player wins or the game ends in a draw.

## Example Gameplay

Here's an example of how a game could play out:

```
Player 1, it's your turn.
_ _ _
_ _ _
_ _ _

Player 1, enter your move (row column): 2 2

Player 2, it's your turn.
_ _ _
_ X _
_ _ _

Player 2, enter your move (row column): 1 1

Player 1, it's your turn.
O _ _
_ X _
_ _ _

Player 1, enter your move (row column): 1 3

Player 2, it's your turn.
O _ X
_ X _
_ _ _

Player 2 wins!
```

## Contributing

Contributions to this project are welcome. If you find any issues or have ideas for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgments

This game was developed as a learning exercise and is based on the classic game of TicTacToe.

## Contact

For any questions or inquiries, please contact [asakai0404@gmail.com](asakai0404@gmail.com).

---

Enjoy playing TicTacToe!
